Name: randomutils
Version: 53.10.19
Release: 1
Epoch: 1
Summary: Utilities for generating 64-bit cryptographic random numbers
URL: https://gitlab.com/hdante/randomutils
License: GPLv3
BuildRequires: zig asciidoc
Recommends: words

%global vname r%{epoch}-%{version}
%global bname %{name}-%{vname}
%global ext tar.gz
%global fname %{bname}.%{ext}

%undefine _disable_source_fetch
%undefine _missing_build_ids_terminate_build

Source0: https://gitlab.com/hdante/%{name}/-/archive/%{vname}/%{fname}
%global SHA256_0 8036cd3a6f06ddd68ddde2b33abb0dfd218cdf559b0313e3ea2c90bf54ac1235

%description
randomutils is a set of tools for generating 64-bit random numbers and is
focused on allowing easy, fast and secure random number generation in shell
scripts. Utilities provided: random, lottery, roll, mempassword.

%prep
echo "%{SHA256_0}  %{SOURCE0}" | sha256sum -c -
%setup -q -n %{bname}

%build
zig build
zig build manpages

%check
zig build test

%install
zig build -p %{buildroot}/usr
zig build -p %{buildroot}/usr manpages

%files
%{_bindir}/random
%{_bindir}/roll
%{_bindir}/lottery
%{_bindir}/mempassword
%{_mandir}/man1/random.1*
%{_mandir}/man1/roll.1*
%{_mandir}/man1/lottery.1*
%{_mandir}/man1/mempassword.1*
%doc README.md
%doc examples
%license COPYING
